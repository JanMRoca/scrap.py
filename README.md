# Scrap.py

### What is it
Web that scraps from several sources your series on demand It can get links from other hosts or download chapters and subtitles itself and upload them to your favourite server.  It's possible to specify preferences such as language, subtitles or favourite servers. 

It stores links and checks their availabiltiy, restoring them in case a chapter goes 404. 

It also improves its scrapping ability by checking how good scrap sources are (how often are links down or the success it got scrapping from there).

It can detect detect if chapters hosts are down or having performance issues and look for other sources.

### How to install
You'll need python3.4 to run this project. Pip3 is also useful for installing used modules (specified in requirements.txt).

To install used modules with pup run:

pip install -r requirements.txt

### How to run
Simply execute as an script.

### Technologies

- Python 3.4
- Flask
- Redis
